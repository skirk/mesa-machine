#!/usr/bin/env bash

set -eux

debootstrap_dir=debootstrap
root_filesystem=img.ext2.qcow2

if [ ! -d "$debootstrap_dir" ]; then
  # Create debootstrap directory.
  # - linux-image-amd64: downloads the kernel image

  packages="openssh-server,
    	    build-essential,
    	    gdb,
    	    gdbserver,
    	    vim,
    	    cmake,
    	    sudo,
    	    git,
	    flex,
	    bison,
	    ninja-build,
	    ca-certificates,
	    python3-setuptools,
	    python3-mako,
	    python3,
	    pkg-config,
	    pkg-config,
	    zlib1g-dev,
	    libexpat1-dev,
	    libelf-dev,
	    libffi-dev,
	    libxml2-dev,
	    dh-autoreconf,
	    libpciaccess-dev,
	    libudev-dev,
	    pciutils,
	    libgcrypt11-dev,
	    perl-openssl-defaults,
	    intltool,
	    gzip"

  #delete whitespace 
  trimmed_packages=$(echo $packages | tr -d ' ')

  sudo debootstrap \
    --include $trimmed_packages \
    stretch \
    "$debootstrap_dir" \
    http://deb.debian.org/debian/ \
  ;
  rm -f "$root_filesystem"
fi

setup_system()
{
  	echo 'root:root' | /usr/sbin/chpasswd
	#add some non-root user
  	user=test
  	id -u $user &>/dev/null || /usr/sbin/useradd $user -m -g sudo -s /bin/bash
  	echo "$user:test" | /usr/sbin/chpasswd
}

setup_build()
{
  	user=test
	export PATH=$PATH:/bin:/sbin
	chown $user:sudo -R /home/$user

	su test -c  'cd ~; echo "export PATH=$PATH:/sbin:/bin" > .bashrc'
	su test -c  'source .bashrc'
	# clone the changes from the host
	su test -c  'mkdir -p ~/mesa-dependencies'
	su test -c  'cd ~; git -C mesa-dependencies pull || git clone https://gitlab.com/skirk/mesa-dependencies.git'
	# build the mesa stack
	su test -c  'mkdir -p ~/mesa-build'
	su test -c  'cd ~/mesa-build && cmake -GNinja ../mesa-dependencies'
	su test -c  'cd ~/mesa-build && sudo -S cmake --build .'
}

export -f setup_system
export -f setup_build

if [ ! -f "$root_filesystem" ]; then

  sudo --preserve-env=BASH_FUNC_setup_system%% chroot "${debootstrap_dir}" /bin/bash -c 'setup_system' 

  #this step needs for the pub key to be in the authorized keys file of the host system
  sudo --preserve-env=BASH_FUNC_setup_build%% chroot "${debootstrap_dir}" /bin/bash -c 'setup_build' 

  # Remount root filesystem as rw.
  # Otherwise, systemd shows:
  #     [FAILED] Failed to start Create Volatile Files and Directories.
  # and then this leads to further failures in the network setup.
  cat << EOF | sudo tee "${debootstrap_dir}/etc/fstab"
/dev/sda / ext4 errors=remount-ro,acl 0 1
EOF

  # Network.
  # We use enp0s3 because the kernel boot prints:
  #     8139cp 0000:00:03.0 enp0s3: renamed from eth0
  # This can also be observed with:
  #     ip link show
  # Without this, systemd shows many network errors, the first of which is:
  #     [FAILED] Failed to start Network Time Synchronization.
cat << EOF | sudo tee "${debootstrap_dir}/etc/network/interfaces.d/00mytest"
auto lo
iface lo inet loopback
allow-hotplug enp0s3
iface enp0s3 inet dhcp
EOF

  # Generate image file from debootstrap directory.
  # Leave 1Gb extra empty space in the image.
  sudo virt-make-fs \
    --format qcow2 \
    --size +1G \
    --type ext2 \
    "$debootstrap_dir" \
    "$root_filesystem" \
  ;
  sudo chmod 666 "$root_filesystem"
fi


#compile the kernel
linux_img=linux/arch/x86_64/boot/bzImage
if [ ! -f "$linux_img" ]; then
  # Build the Linux kernel.
  git clone --depth 1 --branch v4.18 git://git.kernel.org/pub/scm/linux/kernel/git/stable/linux.git
  cd linux
  make x86_64_defconfig
  make kvmconfig
  make -j`nproc`
  cd -
fi

#run the debian setup with the kernel 
qemu-system-x86_64 \
  -drive "file=${root_filesystem},format=qcow2" \
  -kernel "$linux_img" \
  -append 'console=ttyS0 root=/dev/sda' \
  -enable-kvm \
  -vga cirrus \
  -serial mon:stdio \
  -m 2G \
  -device e1000,netdev=net0 \
  -netdev user,id=net0,hostfwd=tcp::5555-:22,hostfwd=tcp::2000-:2000 
;
